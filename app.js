var http = require("http");
var express = require("express");
var path = require("path");
var app = express();

app.use(express.static("public"));

app.get("/", function (req, res) {
	res.sendFile(path.join(__dirname+'/public/index.html'));
});

app.get("/login/", function(req, res){
	res.sendFile(path.join(__dirname+'/public/login.html'));
});

app.get("/register/", function(req, res){
	res.sendFile(path.join(__dirname+'/public/register.html'));
});

app.get("/spot/", function (req, res) {
	res.sendFile(path.join(__dirname+'/public/spot.html'));
});
app.get("/spot/:id", function (req, res) {
	res.sendFile(path.join(__dirname+'/public/spot.html'));
});

app.get("/map/", function(req, res){
	res.sendFile(path.join(__dirname+'/public/map.html'));
});

var server = app.listen(process.env.PORT || 3000, function () {
	var host = server.address().address;
	var port = server.address().port;
	console.log("Example app listening at http://%s:%s", host, port);
});
