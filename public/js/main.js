var Fuse = require("fuse.js");
var Firebase = require("firebase");

var spotsRef = new Firebase("https://sesh-app.firebaseio.com/spots/");
var spots = [];
var spot_coords = {};
spotsRef.on("value", function(snapshot) {
    snapshot.forEach(function(childSnapshot) {
        var spot = childSnapshot.val();
        var spot_id = spot.name+" - "+spot.address;
        spot_coords[spot_id] = "lat="+spot.lat+"&lng="+spot.lng;
        spots.push(spot_id);
    });
});

$(document).ready(function(){
	$("#search").focus();
	$("#search-results").hide();
	$("#cancel-search").hide();
	$("#search").on("keyup",function(){
		$("#home").hide();
		$("#search-container").css("margin-top","2em");
		$("#cancel-search").show();
		$("#search-results").show();
		$("#search-results").empty();
		var f = new Fuse(spots);
		var results = f.search($(this).val());
		if(results.length === 0){
			$("#search-results").append("<p><small>Nothin' :(</small></p>");
		}
		for(var i=0;i<results.length;i++){
			$("#search-results").append("<a href='/map.html?"+spot_coords[spots[results[i]]]+"' class='search-result'>"+spots[results[i]]+"</a>");
		}
	});
	$("#cancel-search").on("click",function(){
		$("#home").show();
		$("#search-container").css("margin-top","12em");
		$("#search-results").hide();
		$("#cancel-search").hide();
	});
});

	